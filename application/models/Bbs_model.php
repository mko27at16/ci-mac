<?php

class Bbs_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function Get_bbs(){
		$hoge = $this->db->get_where('bbs',array('del'=> '0'));
		$array_bbs = $hoge->result_array();
		return $array_bbs;
	}

	public function Get_bbsdetail($id){;
		$hoge = $this->db->get_where('bbs',array('bbs_id' =>$id));
		$array_bbs = $hoge->row_array();
		return $array_bbs;
	}

	public function insert($session_data){
		$this->db->insert('bbs',$session_data);	

	}
	
	public function delete($id){
		$array = array('del' => '1');
		$this->db->where('bbs_id',$id);
		$this->db->update('bbs',$array);
	}

}
