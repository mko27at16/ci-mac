<?php

class Comment_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function Get_id_comment($id){

		$hoge = $this->db->get_where('comment',array('bbs_id' => $id));
		$array_comment = $hoge->result_array();
		return $array_comment;
	}
	
	public function insert($session_data){
		$this->db->insert('comment',$session_data);

	}

}
