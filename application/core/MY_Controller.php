<?php
class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        log_message('debug', 'MY_Controller initilized.');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('date');

    }

}