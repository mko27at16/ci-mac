<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>$title</title>
</head>
<body>

<h1>{$title}</h1>
<h3>{$body}</h3>
<h4>Title:{$bbs_title}</h4>
<h4>Content:{$bbs_body|nl2br}</h4>

<form action="{$ok_url}" method="post">
<input type="submit" name="ok" value="OK"><br><br>
</form>

<a href="{$re_url}">戻る</a>

</body>
</html>