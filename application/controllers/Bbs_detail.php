<?php

class Bbs_detail extends MY_Controller{

	public function index($id){
		if(isset($_SERVER['HTTP_REFERER'])){
			$this->load->model('bbs_model');
			$this->load->model('comment_model');
			$data['arr_bbs'] = $this->bbs_model->Get_bbsdetail($id);
			$data['arr_com'] = $this->comment_model->Get_id_comment($id);
			//$this->load->library('session');
			$data['comment'] = $this->session->userdata('comment');
			$data['is_error'] = $this->session->userdata('is_error');
			$this->smarty->view('bbs_detail.tpl',$data);
			$this->session->sess_destroy();

		}else{
			//$this->load->helper('url');
			redirect('/bbs/');
		}
	}
	public function create($id){
		$p_arr = $this->input->post(array('comment'));
		var_dump($p_arr);
		if($p_arr['comment']){
			//$this->load->library('session');
			//$this->load->helper('date');
			$p_arr['is_error'] = false;
			$p_arr['bbs_id'] = $id;
			$p_arr['date'] = date('Y-m-d H:i:s');
			//p_arr['comment'] = nl2br($p_arr['comment']);ここでやっちゃうと、戻ったときに<br/>が入ってしまう（つまり表示のときだけでいい）
			$this->session->set_userdata($p_arr);
			$array = array('title' => 'コメント確認画面','body' =>'この内容でよろしいでしょうか','bbs_title' => null,'bbs_body' => $p_arr['comment'],'ok_url' => '../../comment/','re_url' =>'../../bbs_detail/'.$id.'/');
			$this->smarty->view('confirm.tpl',$array);
		}else{
			//$this->load->library('session');
			$p_arr['is_error'] = true;
			$this->session->set_userdata($p_arr);
			//$this->load->helper('url');
			redirect('/bbs_detail/'.$id.'/');
		}
	}


}