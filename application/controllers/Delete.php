c<?php
class Delete extends MY_Controller{
	public function index(){
		//$this->load->library('session');//セッションの情報を得る
		$delete_id = $this->session->userdata('delete_id');
		$this->load->model('bbs_model');//Bbs_modelを呼ぶ
		$this->bbs_model->delete($delete_id);
		//$this->load->helper('url');
		$this->session->sess_destroy();
		redirect('/bbs/');
	}
}