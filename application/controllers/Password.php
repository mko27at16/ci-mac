<?php

class Password extends MY_Controller{
	function index($id){
		if(isset($_SERVER['HTTP_REFERER'])){
			//$this->load->library('session');
			$this->session->set_userdata(array('delete_id' => $id));
			$array['is_error'] = $this->session->userdata('is_error');
			$array['is_pass_error'] = $this->session->userdata('is_pass_error');
			$this->smarty->view('password.tpl',$array);
		}else{
			//$this->load->helper('url');
			redirect('/bbs/');
		}
	}

	function password_check(){
		//print("===こっちだよ===");
		//$this->load->library('session');
		$password = $this->input->post('password');
		$this->session->set_userdata(array('is_pass_error' => false));

		if($password){//パスワードが入力されていて、かつパスワードがdelete_idと一致している場合、確認画面に遷移する処理
			//$this->load->library('session');
			$this->session->set_userdata(array('is_error' => false));
			$id = $this->session->userdata('delete_id');
			$this->load->model('bbs_model');
			$delete_arr = $this->bbs_model->Get_bbsdetail($id);
			if($delete_arr['password'] == $password){
				//print('===パスワードが一致しました===');
				$this->session->set_userdata($delete_arr);
				$array = array('title' => '削除画面','body' =>'この内容を削除してよろしいですか','bbs_title' => $delete_arr['title'],'bbs_body' => $delete_arr['body'],'ok_url' => '../../../delete/','re_url' => '../');
				$this->smarty->view("confirm.tpl",$array);
			}else{
				//print('===パスワードが一致しない===');
				//$this->load->library('session');
				$this->session->set_userdata(array('is_pass_error' => true));
				//$this->load->helper('url');
				redirect('/password/'.$id.'/');
			}
		}else{//パスワードが入力されていない
			//print('===パスワードが入力されていない===');
			//$this->load->library('session');
			$id = $this->session->userdata('delete_id');
            $this->session->set_userdata(array('is_error' => true));
            //$this->load->helper('url');
            redirect('/password/'.$id.'/');
		}
	}
}
