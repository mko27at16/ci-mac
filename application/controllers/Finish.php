<?php
class Finish extends MY_Controller{
	public function index(){
		//$this->load->library('session');//セッションの情報を得る
		//$this->load->helper('date');
		$this->load->model('bbs_model');//Bbs_modelを呼ぶ

		$date = $this->session->userdata('date');
		$title = $this->session->userdata('title');
		$body = $this->session->userdata('body');
		$password = $this->session->userdata('password');

		$session_data = array('date'=>$date,'title'=>$title,'body'=>$body,'password'=>$password);
		$this->bbs_model->insert($session_data);//Bbs_modelのinsertメソッドを呼ぶ(sessionの情報を引数で渡す)
		$id = $this->db->insert_id();
		$this->session->sess_destroy();
		//フラグを立てる

		//リダイレクトしてbbs_detailに移動かつその際にidも渡している
		//$this->load->helper('url');
		//redirect('/bbs_detail/'.$id."/");そもそもbbs_detailにいく必要はない？
		redirect('/bbs/');
	}

}