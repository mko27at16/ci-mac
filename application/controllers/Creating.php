<?php

class Creating extends MY_Controller{

    public function index(){

        if(isset($_SERVER['HTTP_REFERER'])){
            //一度登録が押されて、sessionにセットされたdate,title,body,is_errorを変数に格納

            $date = $this->session->userdata('date');
            $title = $this->session->userdata('title');
            $body = $this->session->userdata('body');
            $is_error = $this->session->userdata('is_error');
            $data = array('date'=>$date,'title'=>$title,'body'=>$body,'is_error'=>$is_error);
            $this->smarty->view('creating.tpl',$data);
            //渡した後dataはもういらない
            $this->session->sess_destroy();
        }else{
            redirect('/bbs/');
        }
    }

    public function create(){
        $p_arr = $this->input->post(array('title','body','password'));

        $this->load->helper(array('form','url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','タイトル','required|min_length[4]|max_length[12]');
        $this->form_validation->set_rules('body','内容','required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('password','パスワード','required|min_length[4]|max_length[4]|is_natural');

        if($this->form_validation->run() == false){
            $p_arr['is_error'] = true;
            $this->session->set_userdata($p_arr);
            redirect('/creating/');
        }

        $p_arr['is_error'] = false;
        $p_arr['date'] = date('Y-m-d H:i:s');
        //$p_arr['body'] = nl2br($p_arr['body']);
        $this->session->set_userdata($p_arr);
        $array = array('title' => '確認画面','body' =>'この内容でよろしいでしょうか','bbs_title' => $p_arr['title'],'bbs_body' => $p_arr['body'],'ok_url' => '../../finish/','re_url' => '../../creating/');
        $this->smarty->view('confirm.tpl',$array);  
    }
}
/*
    public function index_f(){
        if(!isset($_SERVER['HTTP_REFERER'])){
            $this->load->helper('url');
            redirect('/bbs/');
        }

        $this->load->library('session');
        //一度登録が押されて、sessionにセットされたdate,title,body,is_errorを変数に格納
        $date = $this->session->userdata('date');
        $title = $this->session->userdata('title');
        $body = $this->session->userdata('body');
        $is_error = $this->session->userdata('is_error');
        $data = array('date'=>$date,'title'=>$title,'body'=>$body,'is_error'=>$is_error);
        $this->smarty->view('creating.tpl',$data);
    
    }
*/








/*
public function create(){
    //登録が押されるとtitle,body,passwordがcreateメソッドにpostされる
    $p_arr = $this->input->post(array('title','body','password'));
    if ($p_arr['title'] && $p_arr['body'] && $p_arr['password']){
        //$this->load->library('session');
        //$this->load->helper('date');
        $p_arr['is_error'] = false;
        $p_arr['date'] = date('Y-m-d H:i:s');
        //postされた情報をsessionにセットしている
        $this->session->set_userdata($p_arr);
        //確認画面に表示する情報を変数に格納している
        $array = array('title' => '確認画面','body' =>'この内容でよろしいでしょうか','bbs_title' => $p_arr['title'],'bbs_body' => $p_arr['body'],'ok_url' => '../../finish/','re_url' => '../../creating/');
        $this->smarty->view('confirm.tpl',$array);  
    }else {
        //$this->load->library('session');
        $p_arr['is_error'] = true;
        $this->session->set_userdata($p_arr);
        //$this->load->helper('url');
        redirect('/creating/');
    }
}
*/
  
