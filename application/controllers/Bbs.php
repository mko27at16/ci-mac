<?php


class Bbs extends MY_Controller{

	public function index()
	{

		$this->load->model('bbs_model');
		$data['title'] = '掲示板';
		$data['arr_bbs'] = $this->bbs_model->Get_bbs();//bbs_modelのget_bbsメソッド（del=0のみを返す）を使用
		$this->smarty->view('index.tpl',$data);
	}
}
